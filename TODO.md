# Cohortes

* CLI script pour ajouter tous les utilisateurs précédemment enregistrés

* Lors de l'authentification

	1. récupérer les groupes dont l'utilisateur est membre

	2. l'ajouter aux cohortes

	3. le supprimer des anciennes cohortes dont il n'est plus membre

* script check-user

	1. en cli, afficher les informations utilisateur

	2. ajouter un rapport (report) check-user

