<?php

/**
 * SGDF authentication plugin upgrade code
 *
 * @package    auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Function to upgrade auth_sgdf.
 * @param int $oldversion the version we are upgrading from
 * @return bool result
 */
function xmldb_auth_sgdf_upgrade($oldversion) {
    global $CFG, $DB;

    // Automatically generated Moodle v3.9.0 release upgrade line.
    // Put any upgrade step following this.

    return true;
}
