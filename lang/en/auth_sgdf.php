<?php

/**
 * Strings for component 'auth_sgdf', language 'en'.
 *
 * @package   auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['auth_sgdf_description'] = 'Authentication based on the web API SGDF.';
$string['pluginname'] = 'SGDF authentication';
$string['privacy:metadata'] = 'The SGDF authentication plugin does not store any personal data.';
$string['check_sgdf_details'] = '<p>The <em>SGDF</em> plugin is a very basic version of OAuth, lacking most of its features.</p>';
$string['check_sgdf_error'] = 'Well, no error. To be deleted.';
$string['check_sgdf'] = 'SGDF';
$string['check_sgdf_ok'] = 'The SGDF plugin is disabled.';
