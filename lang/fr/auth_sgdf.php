<?php

/**
 * Strings for component 'auth_sgdf', language 'fr'.
 *
 * @package   auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['auth_sgdf_description'] = "Authentification basée sur l'API web SGDF.";
$string['pluginname'] = 'Authentication SGDF';
$string['privacy:metadata'] = "L'authentification SGDF ne stocke pas de données personnelles.";
$string['check_sgdf_details'] = '<p>Le plugin <em>SGDF</em> est une version minimaliste de OAuth, sans SSO.</p>';
$string['check_sgdf_error'] = 'Aucune erreur, message à effacer';
$string['check_sgdf'] = 'SGDF';
$string['check_sgdf_ok'] = 'Le plugin auth SGDF est désactivé.';
