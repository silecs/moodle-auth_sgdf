# Plugin auth_sgdf

Plugin d'authentification pour l'API SGDF.

Le plugin nécessite de renseigner des paramètres supplémentaires dans le fichier de configuration générale :

```
$CFG->sgdfparams = [
        'appelantAuth' => '****-****-****-***-******',
        'audience' => '**************************',
        'oauthurl' => 'http://example.com/chemin/token',
        'apiurl' => 'http://example.com/chemin/version/',
];
```
