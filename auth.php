<?php

/**
 * Authentification SGDF, dérivée de "none"
 *
 * @package auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 *
 * @see https://docs.moodle.org/dev/Authentication_plugins
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/authlib.php');

/**
 * Plugin for sgdf authentication.
 */
class auth_plugin_sgdf extends auth_plugin_base {

    /**
     * Constructor.
     */
    public function __construct() {
        global $CFG;
        $this->authtype = 'sgdf';
        $this->config = get_config('auth_sgdf');

        if (! isset($CFG->sgdfparams) || empty($CFG->sgdfparams['oauthurl'])) {
            die ('Paramètres SGDF introuvables dans config.php ($CFG->sgdfparams)');
        }
        $this->params = $CFG->sgdfparams;

        $this->adherentsprocess = null;
    }

     /**
     * Returns true if the username and password work or don't exist and false
     * if the user exists and the password is wrong.
     *
     * @param string $username The username
     * @param string $password The password
     * @return bool Authentication success or failure.
     */
    function user_login ($username, $password) {
        global $SESSION;

        if (! preg_match('/^\d+$/', $username)) {
            echo "Pas un numéro d'adhérent.";
            return false;
        }

        return $this->setAdherentsProcess($username, $password);
    }

    private $authProcess;
    function setAdherentsProcess($username = '', $password = ''): bool
     {
        global $SESSION;
        $authenticated = $this->getAdherentsProcess()->authenticate_oauth($username, $password);
        $SESSION->sgdfaccess = [
            'access_token' => $this->authProcess->access_token,
            'refresh_token' => $this->authProcess->refresh_token,
        ];
        return $authenticated;
    }
    function getAdherentsProcess(): \auth_sgdf\sgdf_adherents
    {
        global $SESSION;
        if ($this->authProcess === null) {
            $this->authProcess = new \auth_sgdf\sgdf_adherents(0);
            if (!empty($SESSION->sgdfaccess)) {
                // Session data exists, so authentication was already successful.
                $this->authProcess->access_token = $SESSION->sgdfaccess['access_token'];
                $this->authProcess->refresh_token = $SESSION->sgdfaccess['refresh_token'];
            } else {
                // Nothing in session, so we can't connect without authenticating first.
                // "L'authentification initiale n'a pas encore eu lieu."
            }
        }
        return $this->authProcess;
    }

    /**
     * C'est la fonction qui initialise les nouveaux utilisateurs à la création
     * Read user information from external source and returns it as array.
     *
     * @return array $result Associative array of user data
     */
    function get_userinfo($username) {
        $adherent = $this->get_user_from_adherent($username);
        //$this->update_user_record($user->username);//FALSE

        return $adherent;
    }

    /**
     * Transforme les infos adhérents de SGDF et profil utilisateur pour Moodle
     *
     * @param string $adherentid
     * @return array
     */
    private function get_user_from_adherent($adherentid): array
    {
        $data = $this->getAdherentsProcess()->get_adherent_info($adherentid);
        $struct = $data['fonctionPrincipale']['structure'];

        $profile = [
            'username' => $adherentid,
            'idnumber' => $data['codeAdherent'],
            'firstname' => $data['prenom'],
            'lastname' => $data['nom'],
            'email' =>  $data['emails'][0],
            'institution' => sprintf('%d:%s', $struct['idInvariantStructure'], $struct['code']),
            'department' => $struct['nom'],
            'country' => 'FR',
            'phone1' => sprintf('%s:%s', $data['telephoneBureau'], $data['telephoneDomicile']),
            'phone2' => sprintf('%s:%s', $data['telephonePortable1'], $data['telephonePortable2']),
        ];

        return $profile;
    }

    
    /**
     * Updates the user's password.
     *
     * called when the user password is updated.
     *
     * @param  object  $user        User table object
     * @param  string  $newpassword Plaintext password
     * @return boolean result
     *
     */
    function user_update_password($user, $newpassword) {
        return false;
    }

    function prevent_local_passwords() {
        return true;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return false;
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return false;
    }

    /**
     * Returns the URL for changing the user's pw, or empty if the default can
     * be used.
     *
     * @return moodle_url
     */
    function change_password_url() {
        return false;
    }

    /**
     * Returns true if plugin allows resetting of internal password.
     *
     * @return bool
     */
    function can_reset_password() {
        return false;
    }

    /**
     * Returns true if plugin can be manually set.
     *
     * @return bool
     */
    function can_be_manually_set() {
        return true;
    }

    /**
     * Returns true if this authentication plugin can edit the users' profile.
     */
    function can_edit_profile() {
        return true;
    }

    function edit_profile_url() {
        return '';
    }

}


