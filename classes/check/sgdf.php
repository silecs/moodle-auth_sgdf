<?php

/**
 * Verifies unsupported sgdf setting
 *
 * @package    auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace auth_sgdf\check;

defined('MOODLE_INTERNAL') || die();

use core\check\result;

class sgdf extends \core\check\check {

    /**
     * A link to a place to action this
     *
     * @return action_link
     */
    public function get_action_link(): ?\action_link {
        return new \action_link(
            new \moodle_url('/admin/settings.php?section=manageauths'),
            get_string('authsettings', 'admin'));
    }

    /**
     * Return result
     * @return result
     */
    public function get_result(): result {
        if (is_enabled_auth('sgdf')) {
            $status = result::ERROR;
            $summary = get_string('check_sgdf_error', 'auth_sgdf');
        } else {
            $status = result::OK;
            $summary = get_string('check_sgdf_ok', 'auth_sgdf');
        }
        $details = get_string('check_sgdf_details', 'auth_sgdf');

        return new result($status, $summary, $details);
    }
}

