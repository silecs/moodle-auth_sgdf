<?php
/**
 * @package    auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace auth_sgdf;

require_once($CFG->dirroot . '/cohort/lib.php');

class sgdf_adherents
{
    public $verbose;
    public $access_token;
    public $refresh_token;
    public $error;
    public $error_description;

    private $params;

    /**
     *
     * @global type $CFG
     * @param int $verbose verbosity
     */
    function __construct(int $verbose)
    {
        global $CFG;
        $this->verbose = $verbose;

        if (! isset($CFG->sgdfparams) || empty($CFG->sgdfparams['oauthurl'])) {
            die ('Paramètres SGDF introuvables dans config.php ($CFG->sgdfparams)');
        }
        $this->params = $CFG->sgdfparams;

        $this->access_token = null;
        $this->refresh_token = null;
        $this->error = null;
        $this->error_description = null;
    }

    public function authenticate_oauth($username, $password) {
        global $SESSION;

        $ch = curl_init();
        $data = [
            'client_id' => $this->params['appelantAuth'],
            'audience' => $this->params['audience'],
            'username' => $username,
            'password' => $password,
            'grant_type' => 'password',
            ];
        curl_setopt_array($ch, [
            CURLOPT_URL => $this->params['oauthurl'],
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => ['Accept: application/json'],
            CURLOPT_POSTFIELDS => http_build_query($data, '', '&')
            ]);
        $res = curl_exec($ch);
        $result = json_decode($res, true);

        if (isset($result['error'])) {
            $this->error = $result['error'];
            $this->error_description = $result['error_description'];
            return false;
        }
        $this->access_token = $result['access_token'];
        $this->refresh_token = $result['refresh_token'];
        return true;
    }

    // debug / diagnostic
    public function get_oauth_error()
    {
        if (! empty($this->error)) {
            return sprintf("ERROR [%s] %s\n", $this->error, $this->error_description);
        }
        return '';
    }

    // debug / diagnostic
    public function get_oauth_tokens()
    {
        $res = '';
        if (! empty($this->access_token)) {
            $res .= sprintf("access_token : [%s]\n", $this->access_token);
        }
        if (! empty($this->refresh_token)) {
            $res .= sprintf("refresh_token: [%s]\n", $this->refresh_token);
        }
        return $res;
    }


    public function get_adherent_info($adherentid)
    {
        $ch = curl_init();
        $headers = [
            'Accept: application/json',
            'idAppelant: ' . $this->params['appelantAuth'],
            'Authorization: bearer ' . $this->access_token,
        ];
        curl_setopt_array($ch, [
            CURLOPT_URL => $this->params['apiurl'] . '/adherents/' . $adherentid,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            ]);
        $res = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new \coding_exception("Curl Error: " . curl_error($ch));
            return null;
        }

        $adherent = json_decode($res, true);
        $data = $adherent['data'];
        return $data;
    }

    /**
     * helper function to display a character on a progressbar
     * @param int $minverb minimal verbosity to display character
     * @param string $text (can be a single char or even a short string)
     */
    private function vecho(int $minverb, string $text)
    {
        if ($this->verbose >= $minverb) echo $text; // progress bar
    }

}
