<?php
/**
 * @package    auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace auth_sgdf;

require_once($CFG->dirroot . '/cohort/lib.php');

class sgdf_cohorts
{
    public $verbose;
    public $sgdfdata; //raw array
    public $sgdfcohorts; //unique cohorts
    public $createcnt = [];

    function __construct(int $verbose)
    {
        global $CFG;
        $this->verbose = $verbose;
        $this->sgdfdata = require($CFG->dirroot . '/auth/sgdf/config/fonctions_cohortes.php');
        $this->createcnt = ['create' => 0, 'noop' => 0];
    }

    /**
     * calcule les cohortes uniques à partir de la liste des codes-fonctions
     */
    public function set_cohorts()
    {
        $res = [];
        foreach ($this->sgdfdata as $code => $cohorts) {
            $res = array_unique(array_merge($res, $cohorts));
        }
        $this->sgdfcohorts = $res;
        return $this;
    }

    public function create_cohorts()
    {
        global $DB;
        $this->vecho(2, "Parsing " . count($this->sgdfcohorts) . " cohorts. \n");

        foreach ($this->sgdfcohorts as $cohortname) {
            if (! $DB->record_exists('cohort', ['name' => $cohortname] )) { // cohort doesn't exist yet
                $newcohort = $this->define_cohort($cohortname);
                $newid = \cohort_add_cohort($newcohort);
                if ( $newid > 0 ) {
                    $this->createcnt['create']++;
                }
            } else { // cohort exists ; must be modified
                $this->createcnt['noop']++;
                }
            }
        return $this;
    }


    /**
     * returns a "newcohort" object from the cohortname (a lot of sparse information, here)
     * @param string $cohortname
     * @return (object) $newcohort
     */
    private function define_cohort($cohortname, $desc=''): object
    {
        $newcohort = [
                        'contextid' => 1,
                        'name' => substr($cohortname, 0, 254),
                        'idnumber' => substr($cohortname, 0, 100),
                        'description' => $desc,
                        'descriptionformat' => 0,
                        'component' => 'auth_sgdf'
                    ];
        return ((object) $newcohort);
    }

    /**
     * helper function to display a character on a progressbar
     * @param int $minverb minimal verbosity to display character
     * @param string $text (can be a single char or even a short string)
     */
    private function vecho(int $minverb, string $text)
    {
        if ($this->verbose >= $minverb) echo $text; // progress bar
    }

}