<?php

/**
 * SGDF authentication plugin
 *
 * @package    auth_sgdf
 * @category   check
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Add security check to make sure this isn't on in production.
 *
 * @return array check
 */
function auth_sgdf_security_checks() {
    return [new auth_sgdf\check\noauth()];
}

