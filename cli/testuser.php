<?php

/**
 * Authentification SGDF
 *
 * @package auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 */

define('CLI_SCRIPT', true);
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/clilib.php');
require_once(__DIR__ . '/../auth.php');

list($options, $unrecognized) = cli_get_params(
    ['help' => false, 'adh' => false, 'pass' => false],
);

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help']) {
    $help =
"Test SGDF authentication

Options:
--help            Print out this help
--adh             adherent number
--pass             password
";
    echo $help;
    die;
}

if ($options['adh'] && $options['pass']) {

    $authsgdf = new \auth_sgdf\sgdf_adherents(2);
    $auth = $authsgdf->authenticate_oauth($options['adh'], $options['pass']);
    if (! $auth) {
        echo $authsgdf->get_oauth_error();
        return false;
    }
    echo $authsgdf->get_oauth_tokens();

    $profile = $authsgdf->get_adherent_info($options['adh']);
    print_r($profile);
    
}
