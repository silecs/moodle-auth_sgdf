<?php
/**
 * Authentification SGDF
 *
 * @package auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 */

use \auth_sgdf\sgdf_cohorts;

define('CLI_SCRIPT', true);
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/clilib.php');

$HELP = <<< ENDHELP
Create SGDF cohorts, from the file: config/fonctions_cohortes.php

Options:
--help            Print out this help
--create          Create the cohorts

ENDHELP;

list($options, $unrecognized) = cli_get_params(
    ['help' => false, 'create' => false ],
);

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help']) {
    echo $HELP;
    return 0;
}

if ($options['create']) {
    $cohortprocess = new sgdf_cohorts(2);
    print_r($cohortprocess->set_cohorts()->sgdfcohorts);
    $cohortprocess->create_cohorts();
    print_r($cohortprocess->createcnt);
}
