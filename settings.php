<?php

/**
 * Admin settings and defaults.
 *
 * @package auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    // Introductory explanation.
    $settings->add(new admin_setting_heading('auth_sgdf/pluginname', '',
        new lang_string('auth_sgdf_description', 'auth_sgdf')));

    // Display locking / mapping of profile fields.
    $authplugin = get_auth_plugin('sgdf');
    display_auth_lock_options($settings, $authplugin->authtype, $authplugin->userfields,
        get_string('auth_fieldlocks_help', 'auth'), false, false);
}
