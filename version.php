<?php

/**
 * Version information
 *
 * @package    auth_sgdf
 * @copyright  2020-2021 SILECS SARL - <guillaume.allegre@silecs.info>
 * derived work from auth_none by Petr Skoda (http://skodak.org) (2011)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2020100804;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2020060900;        // Requires this Moodle version (3.9)
$plugin->component = 'auth_sgdf';       // Full name of the plugin (used for diagnostics)
